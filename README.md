# WebTechDay #2: Webapp for Demo

Webapp for showing temperature and humidity geted from Firebase.

## Install

Required:

- NodeJS: v4.5.0
- NPM: 2.15.9
- Bower: 1.7.9
- Gulp: 3.9.1

Install:

```
npm install
gulp init
```

Run:

```
gulp serve
```

Distribution:

```
gulp dist
```

## Contacts

Jaroslav Khorishchenko (Ukraine)

**Email**: [websnipter@gmail.com](mailto:websnipter@gmail.com)

**Facebook**: [http://fb.com/snipter](http://fb.com/snipter)