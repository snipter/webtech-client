(function(root){
    
    /*============ Regex All ============*/

    RegExp.prototype.execAll = function(string) {
        var match = null;
        var matches = new Array();
        while (match = this.exec(string)) {
            var matchArray = [];
            for (i in match) {
                if (parseInt(i) == i) {
                    matchArray.push(match[i]);
                }
            }
            matches.push(matchArray);
        }
        return matches;
    }

    /*============ Getting code line ============*/

    function codeLine(deep){
        return browserCodeLine(deep);
    }

    function browserCodeLine(deep){
        var stackStr = new Error().stack.toString();
        var regex = /at.+?\:(\d+):(\d+)/g;
        var matches = regex.execAll(stackStr);
        if(!matches.length) return 0;
        if(deep >= matches.length) return 0;
        return matches[deep][1];
    }

    /*============ Log ============*/

    function Log(){
        this.levels = {
            error: 'error',
            warn: 'warning',
            warning: 'warning',
            info: 'info',
            inform: 'info',
            debug: 'debug',
            trace: 'trace'
        }

        this.opt = {
            enabled: true,
            date: false,
            line: true,
            level: 'error'
        }
    }



    Log.prototype = {

        conf: function(conf){
            this.config(conf);
        },

        config: function(conf){
            if(conf.enabled) this.opt.enabled = conf.enabled;
            if(conf.date) this.opt.date = conf.date;
            if(conf.line) this.opt.line = conf.line;
            if(conf.level) this.opt.level = conf.level;
        },

        err: function(data, module, cb){
            this._logRecord(data, "E", module, cb);
        },

        error: function(data, module, cb){
            this._logRecord(data, "E", module, cb);
        },
         
        warn: function(data, module, cb){
            this._logRecord(data, "W", module, cb);
        },

        warning: function(data, module, cb){
            this._logRecord(data, "W", module, cb);
        },
         
        info: function(data, module, cb){
            this._logRecord(data, "I", module, cb);
        },
         
        debug: function(data, module, cb){
            this._logRecord(data, "D", module, cb);
        },
         
        trace: function(data, module, cb){
            this._logRecord(data, "T", module, cb);
        },
         
        _logRecord: function(data, level, module, cb){
            if(!this.opt.enabled) return;
            // checking levels
            if(this.logLevelToInt(level) < this.logLevelToInt(this.opt.level)) return;
            // converting
            if(module && (typeof module === "function")){cb = module; module = null;}

            var text = '';
            // adding date
            if(this.opt.date){
                text += "[" + this.dateToStr(new Date()) + "]";
            }
            // adding level
            text += "["+level+"]";
            // adding module
            if(module && (module != "")) text += "["+module+"]";
            // adding line
            if(this.opt.line){
                text += "[" + codeLine(4) + "]";
            }
            // adding data
            var dataStr = typeof data !== "string" ? JSON.stringify(data) : data;
            text += ": " + dataStr;
            // output text to log
            if(level == 'E'){
                console.error(text);    
            } else if(level == 'W'){
                console.warn(text);
            } else if(level == 'I'){
                console.info(text)
            } else if(level == 'D'){
                console.debug(text)
            } else {
                console.log(text);
            }
            if(cb) cb(data);
        },

        dateToStr: function(date){
            var dd = date.getDate();
            var mm = date.getMonth()+1; //January is 0!
            var yyyy = date.getFullYear();
            if(dd<10){dd='0'+dd};
            if(mm<10){mm='0'+mm} 
            return mm+'/'+dd+'/'+yyyy + ' ' + date.toLocaleTimeString();
        },

        logLevelToInt: function(level){
            if(typeof level === 'number') return level;
            level = level.toLowerCase();
            if(level == "t") return 0;
            if(level == "d") return 1;
            if(level == "i") return 2;
            if(level == "w") return 3;
            if(level == "e") return 4;
            if(level == "trace") return 0;
            if(level == "debug") return 1;
            if(level == "info") return 2;
            if(level == "inform") return 2;
            if(level == "warn") return 3;
            if(level == "warning") return 3;
            if(level == "e") return 4;
            if(level == "err") return 4;
            if(level == "error") return 4;
            return 0;
        }
    }

    root.log = new Log();

})(this);