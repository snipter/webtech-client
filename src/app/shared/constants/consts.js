angular.module('app')
    .constant('consts', {
        local: {
            user: {
                firstAppActivated: 'pando.user.firstAppActivated',
                firstDisplayActivated: 'pando.user.firstDisplayActivated',
                firstGraphicsUploaded: 'pando.user.firstGraphicsUploaded'
            },
            auth: {
                 id: 'pando.auth.id'
                ,email: 'pando.auth.email'
                ,token: 'pando.auth.token'
            },
            display: {
                current: 'pando.display.current'
            },
            urls: {
                redirect: 'pando.urls.redirect'
            }
        },
        notify:{
            device: {
                removed: 'device.removed',
                apps: {
                    updated: 'device.apps.updated'
                }
            },
            auth: {
                'logout': 'auth.logout',
                'logouted': 'auth.logouted',
                'login': 'auth.login',
                'logined': 'auth.logined'
            },
            company: {
                created: 'company.created'
            },
            admin: {
                realtime:{
                    update: 'admin.realtime.update'
                }
            },
            apps: {
                data: {
                    updated: 'app.data.updated'
                },
                action: {
                    add: 'app.action.add',
                    item: {
                        clicked: 'app.action.item.clicked'
                    }
                },
                item: {
                    'add': 'app.item.add'
                },
                items: {
                    'add': 'app.items.add',
                    'updated': 'app.items.updated'
                }
            }
        },
        path: {
            signin: '/signin',
            root: '/'
        }
    });