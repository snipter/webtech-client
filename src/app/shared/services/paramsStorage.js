angular.module('app')
    .service('paramsStorage', function(){
        "ngInject";
        
        var m = 'paramsStorage';
        
        this.set = function(key, val){
            val = JSON.stringify(val);
            localStorage.setItem(key, val);
        }

        this.get = function(key){
            var val = localStorage.getItem(key);
            if(!val) return null;
            if(val == 'undefined') return undefined;
            val = JSON.parse(val);
            return val;
        }

        this.remove = function(key){
            if(this.get(key)) localStorage.removeItem(key);
        }
    })