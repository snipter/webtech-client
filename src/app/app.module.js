angular.module('app', [
     'ngAnimate'
    ,'ngAria'
    ,'ngMessages'
    ,'ngMaterial'
    ,'app.env'
    ,'app.templates'
    ,'firebase'
]);