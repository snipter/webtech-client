angular.module('app')
    .run(function(env){
        "ngInject";
        firebase.initializeApp(env.firebase);
    });