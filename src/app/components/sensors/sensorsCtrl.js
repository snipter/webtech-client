angular.module('app')
	.controller('sensorsCtrl', function($scope, $firebaseObject){
		"ngInject";

		var m = 'sensorsCtrl';

		var sensorId = '-KTJv7CzN0su5rZhdSfu';
		var sensorsListPath = "sensors";
		var sensorCurrentPath = "sensors/" + sensorId + "/current";
		var sensorHistoryPath = 'history/' + sensorId;

		var sensorCurrentRef = firebase.database().ref(sensorCurrentPath);
		$scope.sensor = $firebaseObject(sensorCurrentRef);

		/*============ Init ============*/

		function init(){
			log.debug('init', m);
		}

		init();

		/*============ Functionality ============*/
		
		$scope.sensorLastUpdateStr = function(ts){
			if(!ts) return '';
			return moment(new Date(ts)).format('YY-MM-DD HH:mm:ss');
		}

	});